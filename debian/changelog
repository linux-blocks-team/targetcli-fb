targetcli-fb (1:2.1.53-1) unstable; urgency=medium

  [ Ritesh Raj Sarraf ]
  * Don't hardcode the python minor version

  [ Rafael David Tinoco ]
  * New upstream version 2.1.53
  * No more "fb" in versioning: requires epoch bump
  * d/rules: adjust rules for build, install, man, docs and systemd
  * Add basic DEP8 coverage

 -- Ritesh Raj Sarraf <rrs@debian.org>  Fri, 22 Jan 2021 20:59:16 +0530

targetcli-fb (2.1.fb49-1) unstable; urgency=medium

  * New upstream version 2.1.fb49
  * Refresh patch
  * Fix Vcs URL canonical link

 -- Ritesh Raj Sarraf <rrs@debian.org>  Fri, 02 Aug 2019 20:45:40 +0530

targetcli-fb (2.1.48-1) unstable; urgency=medium

  * d/compat: upgrade debhelper compatibility level to 11
  * d/control: update standards version to 4.2.1
  * d/control: depend on rtslib-fb being at least version 2.1.62
  * d/control: replace DBUS dependency with Python GObject introspection
  * d/patches: rediff patches
  * d/patches: remove patches already merged upstream
  * New upstream version

 -- Christophe Vu-Brugier <cvubrugier@fastmail.fm>  Tue, 28 Aug 2018 22:15:11 +0200

targetcli-fb (2.1.43-2) unstable; urgency=medium

  [ Christian Seiler ]
  * Make sure old config backup directory exists. (Closes: #858459)

  [ Christophe Vu-Brugier ]
  * d/watch: fix the watch file to download from GitHub
  * d/watch: remove "fb" from the version number

  [ Ritesh Raj Sarraf ]
  * Switch packaging to Salsa
  * Set maintainer address to tracker (Closes: #899709)

 -- Ritesh Raj Sarraf <rrs@debian.org>  Fri, 15 Jun 2018 17:14:27 +0545

targetcli-fb (2.1.43-1) unstable; urgency=medium

  [ Christophe Vu-Brugier ]
  * Initial release. (Closes: #838862)

  [ Christian Seiler ]
  * [d18ada9] Use /etc/rtslib-fb-target instead of /etc/target for now
  * [07822ce] debian/control: change maintainer to new LIO target
    packaging mailing list

 -- Christophe Vu-Brugier <cvubrugier@fastmail.fm>  Sat, 24 Sep 2016 21:41:56 +0200
